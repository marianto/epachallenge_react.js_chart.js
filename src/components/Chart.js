import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import {epaData} from '../data/epa';


const Chart = () => {
  const [chartData, setChartData] = useState({});


  const chart = () => {
    let req = [];
    let resCode = [];
    
    epaData.map((epaData,key)=>{

      req.push(epaData.request);
      resCode.push(parseInt(epaData.response_code));
      return epaData;
      
      
     })
    
        setChartData({
          labels: resCode,
          datasets: [
            {
              label: "Distribution of the HTTP answer codes",
              data: req,
              backgroundColor: 'blue',
              borderWidth: 4
            }
          ]
        });
      
     
    console.log(req, resCode);
  };

  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App">
      <h1>Gráficas con Chart.js</h1>
      <div>
        <Line
          data={chartData}
          options={{responsive: true,
            scales: {
              yAxes: [
                {
                  ticks: {
                    autoSkip: true,
                    maxTicksLimit: 10,
                    beginAtZero: true
                  },
                  gridLines: {
                    display: false
                  }
                }
              ],
              xAxes: [
                {
                  gridLines: {
                    display: false
                  }
                }
              ]
            }
          }}
                
            
        />
      </div>
    </div>
  );
};

export default Chart;